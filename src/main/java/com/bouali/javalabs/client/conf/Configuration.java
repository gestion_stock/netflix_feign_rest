package com.bouali.javalabs.client.conf;

public interface Configuration {
	
	void setBaseUrl(String baseUrl);
	
	String getBaseUrl();
	
	void setVersion(String version);
	
	String getVersion();
	
	void setComplementUrl(String complementUrl);
	
	String getComplementUrl();
}
