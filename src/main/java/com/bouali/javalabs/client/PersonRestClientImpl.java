package com.bouali.javalabs.client;

import java.util.List;

import com.bouali.javalabs.client.builder.PersonParamBuilder;
import com.bouali.javalabs.client.conf.Configuration;
import com.bouali.javalabs.client.entities.Person;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

public class PersonRestClientImpl implements PersonRestClient {
	
	private PersonRestClient restClient;
	
	private String baseUrl;
	
	private Configuration config;
	
	public PersonRestClientImpl(Configuration config) {
		this.config = config;
		initBaseUrl();
		restClient = Feign.builder()
					 .encoder(new JacksonEncoder())
					 .decoder(new JacksonDecoder())
					 .target(PersonRestClient.class, baseUrl);
	}
	
	private void initBaseUrl() {
		baseUrl = config.getBaseUrl() + config.getVersion() + config.getComplementUrl();
	}

	public Person createPerson(Person person) {
		return restClient.createPerson(person);
	}

	public Person updatePerson(Person person) {
		return restClient.updatePerson(person);
	}

	public void deletePerson(Long id) {
		restClient.deletePerson(id);
	}

	public List<Person> getAll() {
		return restClient.getAll();
	}

	public List<Person> getByName(String name) {
		return restClient.getByName(name);
	}

	public List<Person> getByFamilyName(String familyName) {
		return restClient.getByFamilyName(familyName);
	}

	public List<Person> getPersons(PersonParamBuilder params) {
		return restClient.getPersons(params);
	}

}
