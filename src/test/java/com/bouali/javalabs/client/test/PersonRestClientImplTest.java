package com.bouali.javalabs.client.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.bouali.javalabs.client.PersonRestClient;
import com.bouali.javalabs.client.PersonRestClientImpl;
import com.bouali.javalabs.client.builder.Builder;
import com.bouali.javalabs.client.builder.PersonParamBuilder;
import com.bouali.javalabs.client.conf.Configuration;
import com.bouali.javalabs.client.conf.ConfigurationImpl;
import com.bouali.javalabs.client.entities.Person;

public class PersonRestClientImplTest {

	private PersonRestClient restClient;
	private Configuration conf;
	
	@Before
	public void setUp() {
		conf = new ConfigurationImpl();
		conf.setBaseUrl("http://localhost:8082/");
		conf.setVersion("v1");
		conf.setComplementUrl("/person");
		restClient = new PersonRestClientImpl(conf);
	}
	
	@Test
	public void testGetAllPersons() {
		List<Person> persons = restClient.getAll();
		System.out.println(persons.size());
		assertTrue(persons.size() == 7);
	}
	
	@Test
	@Ignore
	public void createPerson() {
		Person person = new Person();
		person.setName("TOTO");
		person.setFamilyName("TATA");
		person.setAdress("Adresse");
		person.setMail("person@person.com");
		Person createdPerson = restClient.createPerson(person);
		assertTrue(createdPerson.getIdPerson() != null && "TOTO".equals(createdPerson.getName()));
	}
	
	@Test
	public void testParams() {
		PersonParamBuilder params = new Builder()
									.param1("aaaaa")
									.param2("bbbb")
									.build();
		List<Person> persons = restClient.getPersons(params);
		assertTrue(persons.size() == 7);
	}

}
